import React, { useState } from 'react';
import "./BasicTest.css"

function BasicTest() {

  const [ipAddress, setIpAddress] = useState(['', '', '', '']);
  const [inputValue, setInputValue] = useState('');

  // const handleSubmit = (event) => {
  //   event.preventDefault();
  //   console.log(inputValue);
  // }

  // const handleInputChange = (event) => {
  //   setInputValue(event.target.value);
  // }


  // function handleChange(event, index) {
  //   const value = event.target.value;
  //   setIpAddress(prevIpAddress => {
  //     const newIpAddress = [...prevIpAddress];
  //     newIpAddress[index] = value;
  //     return newIpAddress;
  //   });
  // }

  const [selectedOption, setSelectedOption] = useState("");
  const [showModal, setShowModal] = useState(false);

  function handleChange(event) {
    setSelectedOption(event.target.value);
  }

  function handleOptionClick(event) {
    setShowModal(true);
  }


  return (
<div class="parent-container">
  {/* <div class="child-div">something</div>
  <div class="child-div">something</div>
  <div class="child-div-special">something</div> */}
  {/* <div>
      <label htmlFor="ip-address">IP Address:</label>
      <input
        type="number"
        id="ip-address-1"
        name="ip-address-1"
        value={ipAddress[0]}
        onChange={event => handleChange(event, 0)}
        pattern="^([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$"
        maxLength="3"
        required
      />
      .
      <input
        type="number"
        id="ip-address-2"
        name="ip-address-2"
        value={ipAddress[1]}
        onChange={event => handleChange(event, 1)}
        pattern="^([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$"
        maxLength="3"
        required
      />
      .
      <input
        type="number"
        id="ip-address-3"
        name="ip-address-3"
        value={ipAddress[2]}
        onChange={event => handleChange(event, 2)}
        pattern="^([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$"
        maxLength="3"
        required
      />
      .
      <input
        type="number"
        id="ip-address-4"
        name="ip-address-4"
        value={ipAddress[3]}
        onChange={event => handleChange(event, 3)}
        pattern="^([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$"
        maxLength="3"
        required
      />
      <button onClick={() => console.log(ipAddress.join('.'))}>Submit</button>
    
    </div> */}


{/* <div> */}
{/* <div>
      <label htmlFor="ip-address">IP Address:</label>
      <input
        type="text"
        id="ip-address-1"
        name="ip-address-1"
        value={ipAddress[0]}
        onChange={event => handleChange(event, 0)}
        maxLength={3}
        required
      />
      .
      <input
        type="text"
        id="ip-address-2"
        name="ip-address-2"
        value={ipAddress[1]}
        onChange={event => handleChange(event, 1)}
        maxLength={3}
        required
      />
      .
      <input
        type="text"
        id="ip-address-3"
        name="ip-address-3"
        value={ipAddress[2]}
        onChange={event => handleChange(event, 2)}
        maxLength={3}
        required
      />
      .
      <input
        type="text"
        id="ip-address-4"
        name="ip-address-4"
        value={ipAddress[3]}
        onChange={event => handleChange(event, 3)}
        maxLength={3}
        required
      />
      <button onClick={() => console.log(ipAddress.join('.'))}>Submit</button>
    </div>

</div> */}
{/* <form onSubmit={handleSubmit}>
      <label>
        IP address:
        <input 
          type="text" 
          value={inputValue} 
          onChange={handleInputChange} 
          pattern="^([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]|1[0-9][0-9])$"
          required
        />
      </label>
      <button type="submit">Submit</button>
    </form> */}

<div>
      <select value={selectedOption} onChange={handleChange}>
        <option value="option1" onClick={handleOptionClick}>Option 1</option>
        <option value="option2" onClick={handleOptionClick}>Option 2</option>
        <option value="option3" onClick={handleOptionClick}>Option 3</option>
      </select>
    </div>
    {showModal && (
  <div>
    <h2>Modal Content</h2>
    <button onClick={() => setShowModal(false)}>Close Modal</button>
  </div>
)}
</div>
  );
}

export default BasicTest;
