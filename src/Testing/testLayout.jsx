import React from 'react'

export default function testLayout({ sidebar, children }) {
  return (
    <div>
    <div className="sidebar">{sidebar}</div>
    <div className="main-content">{children}</div>
  </div>
  )
}
