import React from 'react'

export default function testMainContent() {
  return (
    <Layout
    sidebar={<Sidebar />}
  >
    <MainContent />
  </Layout>
  )
}
