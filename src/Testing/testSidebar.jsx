import React from 'react'

export default function testSidebar() {


    const [dropdownOpen, setDropdownOpen] = useState(false);
    
    const toggleDropdown = () => {
        setDropdownOpen(!dropdownOpen);
      };

  return (
    <div>
        <div className="sidebar">
  <button onClick={toggleDropdown}>Open dropdown</button>
  {dropdownOpen && (
    <ul>
      <li>Item 1</li>
      <li>Item 2</li>
      <li>Item 3</li>
    </ul>
  )}
</div>

    </div>
  )
}
