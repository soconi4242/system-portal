import React from 'react'
import "./Login.css"
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';


export default function Login() {

  const [user, setuser] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();


  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post('http://127.0.0.1:8000/api/admin/login', {
        user,
        password,
      });
      console.log(response);
      toast.success("Login successfully")
      navigate('/admin-dashboard')
    } catch (error) {
      console.error(error);
      toast.error("Invalid credentials")
    }
  };


  return (
    <div className='LoginPageTop'>
      <div className="container LoginPageContainer">

        {/* <img className='LoginPageImage' src="https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="" /> */}
        <img className='d-none d-md-block LoginPageImage' src="https://img.freepik.com/premium-vector/illustration-vector-graphic-cartoon-character-login_516790-1261.jpg?w=2000" alt="" />

      <form className='LoginForm border'>
      <h2 className='pb-3 text-center'>Login</h2>
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" onChange={(e) => setuser(e.target.value)} placeholder='Email'></input>
    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" onChange={(e) => setPassword(e.target.value)} placeholder='Password' ></input>
  </div>
  {/* <div class="mb-3 form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1"></input>
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div> */}
  <div className='d-flex flex-column justify-content-between buttonsDiv'>
  <button type="button" class="loginButton py-2" onClick={handleSubmit} >Login</button>
  <p className='text-center'>Not a member?<span onClick={(e)=>{
            navigate('/registration');
          }} className='subtextLoginButton'>Sign Up</span></p>
  </div>


</form>
      </div>

      <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>

    </div>
  )
}
