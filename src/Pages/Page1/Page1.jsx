import React from 'react'
import Sidebar from '../../Components/Sidebar/Sidebar'
import DBAreaChart from '../../Components/DBAreaChart/DBAreaChart'
import DBLineChart from '../../Components/DashBoardLineChart/DBLineChart'
import "./Page1.css"

export default function Page1() {
    return (
        <div className='Page1Top'>
            <div className='AdminDashboardTop'>
                <Sidebar className="SidebarAdminPage"></Sidebar>

                <div className="AdminDashboardContentsTop container">

                    <div className="Page1Tabs">
                        <div className="TabElements border-bottom row m-0">
                            <div className="col px-2 pt-2 border-end">Overview</div>
                            <div className="col px-2 pt-2 border-end">Settings</div>
                            <div className="col px-2 pt-2 border-end">License info</div>
                            <div className="col px-2 pt-2 border-end">System Information</div>
                            <div className="col px-2 pt-2 border-end">Version info</div>
                            <div className="col px-2 pt-2 border-end">Run command</div>
                            <div className="col px-2 pt-2 border-end">Fabrics</div>
                            <div className="col pt-2">Rack view</div>
                        </div>
                        <div className="TabElementsMiddle pt-3 border-bottom d-flex justify-content-between px-5">
                            <p className="TabMiddleText">
                                Partition <strong>base - CDAC Cluster</strong>
                            </p>
                            <i class="fa-solid fa-up-right-and-down-left-from-center"></i>
                        </div>
                        <div className="TabElementsBottom py-2 d-flex justify-content-end pe-3">
                            <div className="TabIcons">
                            <i class="fa-solid px-2 fa-circle-plus"></i>
                            <i class="fa-solid px-2 fa-rotate-left"></i>
                            <i class="fa-solid px-2 fa-gear"></i>
                            </div>
                        </div>
                    </div>

                    <div className="AdminChartMiddle d-flex justify-content-around py-5">
                        <div className="AdminChartsLeft mx-2">
                            <DBLineChart></DBLineChart>
                        </div>
                        <div className="AdminChartsRight mx-2">
                            <DBAreaChart></DBAreaChart>
                        </div>
                    </div>

                    <div className="AdminChartMiddle d-flex justify-content-around pb-3">
                        <div className="AdminChartsRight mx-2">
                            <DBAreaChart></DBAreaChart>
                        </div>
                        <div className="AdminChartsLeft mx-2">
                            <DBLineChart></DBLineChart>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    )
}
