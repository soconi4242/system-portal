import React from 'react'
import Sidebar from '../../Components/Sidebar/Sidebar'
import "./AddUser.css"
import AddUserTable from '../../Components/AddUserTable/AddUserTable'
import MultiPageUserForm from '../../Components/MultiPageUserForm/MultiPageUserForm'
import { useState } from 'react'


export default function AddUser() {

  const [isToggleOn, setIsToggleOn] = useState(false);

  const hideForm = () => {
    setIsToggleOn(!isToggleOn);
  };

  return (
    <div className='AdminDashboardTop'>
        <Sidebar className="SidebarAdminPage"></Sidebar>

        <div className="AdminDashboardContentsTop container">
          {/* <DashboardCards></DashboardCards>

          <div className="AdminChartMiddle d-flex justify-content-around py-5">
          <div className="AdminChartsLeft mx-2">
            <DBLineChart></DBLineChart>
          </div>
          <div className="AdminChartsRight mx-2">
            <DBAreaChart></DBAreaChart>
          </div>
        </div>

        <div className="AdminDashboardUserTable mx-2">
          <DBUserTable></DBUserTable>
        </div> */}

        <div className="row">

          <div className="col-4">
          <div className="AdminAddUserTable">
            <AddUserTable></AddUserTable>
          </div>
          <div className="AdminAddUserButton py-3">
            <button className='AddUserButton me-3 specialButton' onClick={hideForm}>
            <i class="fa-solid fa-plus"></i>
            <span className='ms-2'>ADD</span>
            </button>
          </div>
          </div>

          <div className="col-7 d-flex flex-column">
            <div className="AddUserRightTop">
              <div className="AddUserRightTopText d-flex justify-content-between px-2 py-2 align-item-center">
                <span>
                  User
                </span>
                <i class="fa-solid fa-up-right-and-down-left-from-center"></i>
              </div>

              <hr className='p-0 m-0' />

              <div className="AddUserSearch py-2 d-flex justify-content-between px-3">
                

                <div className="AddUserSearchBar border-bottom d-flex justify-content-between mx-3 w-100">
                  <div>
                  <i class="fa-solid fa-magnifying-glass"></i>
                  <input className='border-0 class ms-2' type="text" name="" id="" placeholder='Search settings inputs' />
                  
                  </div>
                  <i class="fa-solid fa-xmark"></i>
                </div>

                <i class="fa-solid fa-chevron-up"></i>

                
              </div>

            </div>
            <div className="AddUserRightMiddle px-3 py-3 text-start">
              <span className="AddMiddleText">
                JUMP TO
              </span>
              <div className="AddMiddleButtonDiv my-3">
                <button className="AddMiddleButton specialButton">
                  <span className='me-2'>Project manager</span>
                <i class="fa-solid fa-chevron-right"></i>
                </button>
                <button className='ms-1 specialButton'>
                <i class="fa-solid fa-chevron-down"></i>
                </button>
              </div>
            </div>
            <div className="AddUserRightBottom">
            {isToggleOn ? <MultiPageUserForm></MultiPageUserForm> : <></>}
              
            </div>
          </div>
        </div>





        </div>

    </div>
  )
}
