import React from 'react'
import Sidebar from '../../Components/Sidebar/Sidebar'
import DBAreaChart from '../../Components/DBAreaChart/DBAreaChart'
import DBLineChart from '../../Components/DashBoardLineChart/DBLineChart'
import "./Page2.css"
import Slider from '../../Components/Slider/Slider'
import CustomSlider from '../../Components/Slider/Slider'

export default function Page2() {
    return (
        <div className='Page2Top'>
            <div className='AdminDashboardTop'>
                <Sidebar className="SidebarAdminPage"></Sidebar>

                <div className="AdminDashboardContentsTop container">

                    <div className="Page2Tabs">
                        <div className="TabElements border-bottom row m-0">
                            <div className="col px-2 pt-2 border-end">Overview</div>
                            <div className="col px-2 pt-2 border-end">Settings</div>
                            <div className="col px-2 pt-2 border-end">License info</div>
                            <div className="col px-2 pt-2 border-end">System Information</div>
                            <div className="col px-2 pt-2 border-end">Version info</div>
                            <div className="col px-2 pt-2 border-end">Run command</div>
                            <div className="col px-2 pt-2 border-end">Fabrics</div>
                            <div className="col pt-2">Rack view</div>
                        </div>
                        <div className="TabElementsMiddle pt-3 border-bottom d-flex justify-content-between px-5">
                            <p className="TabMiddleText">
                                Partition <strong>base - CDAC Cluster</strong>
                            </p>
                            <i class="fa-solid fa-up-right-and-down-left-from-center"></i>
                        </div>
                        <div className="TabElementsBottom py-2 d-flex justify-content-end pe-3">
                            <div className="TabIcons">
                            <i class="fa-solid px-2 fa-circle-plus"></i>
                            <i class="fa-solid px-2 fa-rotate-left"></i>
                            <i class="fa-solid px-2 fa-gear"></i>
                            </div>
                        </div>
                    </div>

                    <div className="Page2Charts py-5">
                        <div className="Page2ChartsTop row m-0 d-flex justify-content-around">
                            <div className="Page2ChartTopLeft col-4 text-start px-3 border py-2 d-flex">
                                <div className="chartsTopLeft pe-2 d-flex flex-column justify-content-between">
                                <span className="ChartTopItems">Uptime:  </span>
                                <span className="ChartTopItems">Nodes: </span>
                                <span className="ChartTopItems">GPU Units:  </span>
                                <span className="ChartTopItems">Devices:  </span>
                                <span className="ChartTopItems">Jobs:  </span>
                                <span className="ChartTopItems">Phase load:  </span>
                                </div>
                                <div className="chartsTopRight d-flex flex-column justify-content-between align-items-start">
                                <span>45 days 3 hours 7 minutes</span>
                                <span>503 7 2</span>
                                <span>38 0 0</span>
                                <span>64 0 0</span>
                                <span>45 running 67 waiting</span>
                                <span>783 A</span>
                                </div>
                                
                            </div>
                            <div className="Page2ChartTopRight col-7 border py-2">
                                <div className='d-flex justify-content-between px-2'>
                                    <p>CPU Cores: </p>
                                    <CustomSlider></CustomSlider>
                                </div>
                                <div className='d-flex justify-content-between px-2'>
                                    <p>GPUs: </p>
                                    <CustomSlider></CustomSlider>
                                </div>
                                <div className='d-flex justify-content-between px-2'>
                                    <p>Memory: </p>
                                    <CustomSlider></CustomSlider>
                                </div>
                                <div className='d-flex justify-content-between px-2'>
                                    <p>Users: </p>
                                    <CustomSlider></CustomSlider>
                                </div>
                                <div className='d-flex justify-content-between px-2'>
                                    <p>CPU Usage: </p>
                                    <CustomSlider></CustomSlider>
                                </div>
                                <div className='d-flex justify-content-between px-2'>
                                    <p>Occupation rate: </p>
                                    <CustomSlider></CustomSlider>
                                </div>
                            </div>
                        </div>
                        <div className="Page2ChartsMiddle row m-0">
                            <div className="Page2ChartTopLeft col-4">

                            </div>
                            <div className="Page2ChartTopRight col-7">

                            </div>
                        </div>
                        <div className="Page2ChartsBottom row m-0">

                        </div>
                    </div>

                </div>

            </div>
        </div>
    )
}
