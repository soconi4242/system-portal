import React from 'react'
import Sidebar from '../../Components/Sidebar/Sidebar'
import DBUserTable from '../../Components/DashboardUserTable/DBUserTable'
import { useState } from 'react';
import { toast , ToastContainer} from 'react-toastify';
import {isIP, isIPv4} from 'is-ip';
import "./NodeDefination.css"

export default function NodeDefination() {

    const [isAddNodeVisible, setIsAddNodeVisible] = useState(false);
    const [isCustomNodeVisible, setIsCustomNodeVisible] = useState(false);
    const [isDeleteNodeVisible, setIsDeleteNodeVisible] = useState(false);
    const [formData, setFormData] = useState({
      email: '',
      ip: '',
      groups: '',
      managementIp: '',
      ManagementUsername: '',
      MacAddress: '',
      password: '',
    });

    const [formData1, setFormData1] = useState({
      email: '',
      firstip: '',
      lastip: '',
      groups: '',
      managementIp: '',
      ManagementUsername: '',
      password: '',
    });

  
    const handleInputChange = (event) => {
      const { name, value } = event.target;
      setFormData({ ...formData, [name]: value });
    };
  
    const clearPage = (event) => {
      setFormData({
        email: '',
        ip: '',
        groups: '',
        managementIp: '',
        ManagementUsername: '',
        MacAddress: '',
        password: ''
      });
    };
  
    const handleSubmit = (event) => {
      event.preventDefault();
      if (formData.email == "" || formData.ip == "" || formData.groups == "" || formData.managementIp == "" || formData.ManagementUsername == "" || formData.password == "" || formData.MacAddress == "") {
        // some input fields are empty, show an error message
          console.log('Please fill all input fields.');
        toast.error("Please enter all the fields")


      } else {

                // all input fields are filled, do something
                console.log('All input fields are filled.');
                // console.log(isIP(formData.ip))
                // console.log(isIP(formData.managementIp))
                  console.log(formData);
                  setFormData({
                    email: '',
                    ip: '',
                    groups: '',
                    managementIp: '',
                    ManagementUsername: '',
                    MacAddress: '',
                    password: '',
                  });
      }
      

    };

    const handleSubmit1 = (event) => {
      event.preventDefault();
      if (formData1.email == "" || formData1.firstip == ""||formData1.lastip == ""  || formData1.groups == "" || formData1.managementIp == "" || formData1.ManagementUsername == "" || formData1.password == "") {
        // some input fields are empty, show an error message
          console.log('Please fill all input fields.');
        toast.error("Please enter all the fields")


      } else {

                // all input fields are filled, do something
                console.log('All input fields are filled.');
                // console.log(isIP(formData.ip))
                // console.log(isIP(formData.managementIp))
                  console.log(formData);
                  setFormData({
                    email: '',
                    ip: '',
                    groups: '',
                    managementIp: '',
                    ManagementUsername: '',
                    MacAddress: '',
                    password: '',
                  });
      }
      

    };
  


    const AddNode = (event) => {
        setIsAddNodeVisible(!isAddNodeVisible);
      };

  return (
    <div className='NodeDefinationTop'>
                    <div className='AdminDashboardTop'>
                <Sidebar className="SidebarAdminPage"></Sidebar>

                <div className="AdminDashboardContentsTop container">

                    <div className="AdminNodeDefinationTable">
                        <DBUserTable></DBUserTable>
                    </div>

                    <div className="row">
                      <div className="col">
                      <button className="addNodeButton button-50" onClick={AddNode}>
                            Add Node
                        </button>
                        {isAddNodeVisible && 
                        <div className="AddNodeHidden mt-2">

                                            {/* <!-- Button trigger modal --> */}
<button type="button" class="NodeDefinationButtons button-50" data-bs-toggle="modal" data-bs-target="#addSingleNode">
Single node
</button>

                                            {/* <!-- Button trigger modal --> */}
<button type="button" class="NodeDefinationButtons button-50" data-bs-toggle="modal" data-bs-target="#addMultipleNodes">
Multiple nodes
</button>

                        </div>
                    }
                      </div>
                      <div className="col">
                      <button className="CustomNodeButton button-50" onClick={()=>{
                          setIsCustomNodeVisible(!isCustomNodeVisible)
                        }}>
                            Custom Node
                        </button>
                        {isCustomNodeVisible && 
                        <div className="AddNodeHidden mt-2">

                                            {/* <!-- Button trigger modal --> */}
<button type="button" class="NodeDefinationButtons button-50" data-bs-toggle="modal" data-bs-target="#addSingleNode">
Single node
</button>

                                            {/* <!-- Button trigger modal --> */}
<button type="button" class="NodeDefinationButtons button-50" data-bs-toggle="modal" data-bs-target="#addMultipleNodes">
Multiple nodes
</button>

                        </div>
                    }
                      </div>
                      <div className="col">

                      <button className="DeleteNodeButton button-50" onClick={()=>{
                          setIsDeleteNodeVisible(!isDeleteNodeVisible)
                        }}>
                            Delete Node
                        </button>
                        
                    {isDeleteNodeVisible && 
                        <div className="AddNodeHidden mt-2">

                                            {/* <!-- Button trigger modal --> */}
<button type="button" class="NodeDefinationButtons button-50" data-bs-toggle="modal" data-bs-target="#addSingleNode">
Single node
</button>

                                            {/* <!-- Button trigger modal --> */}
<button type="button" class="NodeDefinationButtons button-50" data-bs-toggle="modal" data-bs-target="#addMultipleNodes">
Multiple nodes
</button>

                        </div>
                    }
                      </div>
                    </div>

                    <div className="AdminNodeDefinationDropdownButtons w-100 d-flex justify-content-around">


                    </div>
                    <div className="AdminNodeDefinationDropdownContent d-flex justify-content-around">



                    </div>
                </div>


{/* <!-- Modal --> */}
<div class="modal fade" id="addSingleNode" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       
      <form>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Hostname</label>
    <input type="text" class="form-control"  name="email" value={formData.email} onChange={handleInputChange}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">IP Address</label>
    <input type="text" class="form-control"  name="ip" value={formData.ip} onChange={handleInputChange}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Groups</label>
    <input type="text" class="form-control"  name="groups" value={formData.groups} onChange={handleInputChange}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Management IP</label>
    <input type="text" class="form-control"  name="managementIp" value={formData.managementIp} onChange={handleInputChange}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Management Username</label>
    <input type="text" class="form-control" name="ManagementUsername" value={formData.ManagementUsername} onChange={handleInputChange}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">MAC address</label>
    <input type="text" class="form-control" name="MacAddress" value={formData.MacAddress} onChange={handleInputChange}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="exampleInputPassword1" class="text-start">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" name="password" value={formData.password} onChange={handleInputChange}></input>
  </div>

</form>


      </div>
      <div class="modal-footer">
        <button type="button" class="NodeDefinationButtons button-55" data-bs-dismiss="modal">Close</button>
        <button type="button" class="NodeDefinationButtons button-55" onClick={handleSubmit}>Save changes</button>
      </div>
    </div>
  </div>
</div>

{/* <!-- Modal --> */}
<div class="modal fade" id="addMultipleNodes" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       
      <form>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Hostname</label>
    <input type="text" class="form-control"  name="email" value={formData1.email} onChange={handleInputChange}></input>
 
  </div>
  <p className='text-start'>Provide an IP range</p>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Start IP</label>
    <input type="text" class="form-control"  name="ip" value={formData1.ip} onChange={handleInputChange}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Last IP</label>
    <input type="text" class="form-control"  name="ip" value={formData1.ip} onChange={handleInputChange}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Groups</label>
    <input type="text" class="form-control"  name="groups" value={formData1.groups} onChange={handleInputChange}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Management IP</label>
    <input type="text" class="form-control"  name="managementIp" value={formData1.managementIp} onChange={handleInputChange}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">Management Username</label>
    <input type="text" class="form-control" name="ManagementUsername" value={formData1.ManagementUsername} onChange={handleInputChange}></input>
 
  </div>
  <div class="mb-3 text-start">
    <label for="" class="text-start">MAC address</label>
    {/* <input type="text" class="form-control" name="MacAddress" value={formData1.MacAddress} onChange={handleInputChange}></input> */}
    <input type="file" class="form-control" name="MacAddress" id="" />
  </div>
  <div class="mb-3 text-start">
    <label for="exampleInputPassword1" class="text-start">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" name="password" value={formData1.password} onChange={handleInputChange}></input>
  </div>

</form>


      </div>
      <div class="modal-footer">
        <button type="button" class="NodeDefinationButtons button-55" data-bs-dismiss="modal">Close</button>
        <button type="button" class="NodeDefinationButtons button-55" onClick={handleSubmit1}>Save changes</button>
      </div>
    </div>
  </div>
</div>

            </div>

            <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
    </div>
  )
}
